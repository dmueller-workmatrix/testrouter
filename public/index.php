<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');

require_once('../Controller.php');
require_once('../Router.php');

function correct($variables) {
	 echo 'called Controller:function '.$variables['id'];
}

$router = new Router('/simpleroute/public/');

$correct = array('api','user','$id','file','$fileid');
$n = sizeof($correct);

for ($i=0; $i < 300; $i++) {
	$testroute = $correct;
	$testroute[rand()%$n] = 'falseroute'.$i;
	$testroute = join('/', $testroute);

	$router->get($testroute, 'Controller:function'.$i);
}
$router->get('/api/user/$id/file/$fileid', 'correct');

// $router->all('/api/closure/$id', function($params) { die('We got id ' . $params['id']); });
// $router->all('/', function($params) { die('Catchall'); });

echo $router->run();

