<?php

/**
 * Simple Router that tries to stay away from preg_match and call_user_func_array
 *
 * $router = new Router();
 * $router->get('/api/user/$id/$fileid', 'Controller:function');
 * $router->all('/api/closure/$id', function($params) { die('We got id ' . $params['id']); });
 * $router->post('/api/files/$id/file/$fname', 'Controller::staticfunction');
 * $router->put('/api/filesagain/$id/file/$fname', 'Controller:publicfunction');
 * $router->get('/api/user', 'Controller::functionAlsoPossibleButNotCalled');
 */
class Router
{
	protected $routes;
	protected $rootDir;

	public function __construct($rootDir = '') {

		$this->routes = array(
			'get'=>array(),
			'post'=>array(),
			'put'=>array(),
			'delete'=>array(),
			'patch'=>array(),
			'all'=>array(),
		);

		$this->rootDir = $rootDir;
	}

	public function get($route, $callable) {
		$this->routes['get'][] = array($route => $callable);
	}
	public function post($route, $callable) {
		$this->routes['post'][] = array($route => $callable);
	}
	public function put($route, $callable) {
		$this->routes['put'][] = array($route => $callable);
	}
	public function delete($route, $callable) {
		$this->routes['delete'][] = array($route => $callable);
	}
	public function patch($route, $callable) {
		$this->routes['patch'][] = array($route => $callable);
	}
	public function all($route, $callable) {
		$this->routes['all'][] = array($route => $callable);
	}

	public function run() {
		$requestUri = $_SERVER['REQUEST_URI'];
		return $this->dispatchRoute($requestUri);
	}

	private function dispatchRoute($path) {
		$routes = array();

		if (false !== ($data = apcu_fetch(urlencode($path)))) {
			return $this->dispatch($data[0], $data[1]);
		}

		// filter the routes by request method
		switch($_SERVER['REQUEST_METHOD']) {
			default:
			case 'GET':
				$routes = array_merge($this->routes['get'], $this->routes['all']);
				break;
			case 'POST':
				$routes = array_merge($this->routes['post'], $this->routes['all']);
				break;
			case 'PUT':
				$routes = array_merge($this->routes['put'], $this->routes['all']);
				break;
			case 'DELETE':
				$routes = array_merge($this->routes['delete'], $this->routes['all']);
				break;
			case 'PATCH':
				$routes = array_merge($this->routes['patch'], $this->routes['all']);
				break;
		}

		// explode and format the currently active routes
		$routesArray = array();
		foreach ($routes as $route) {
			foreach ($route as $route => $callable) {
				$route = $this->rootDir . $route;
				if ($route[0] == '/') {
					$route = substr($route, 1);
				}
				$route = str_replace('//', '/', $route);

				$routesArray[] = array($callable, explode('/', $route));
			}
		}

		// find the route of the current path
		$path = substr($path, 1);
		$pathArray = explode('/', $path);

		// now remove nonmatching routes one by one
		for ($i=0, $n=sizeof($pathArray); $i < $n; $i++) {
			for ($j=0, $m=sizeof($routesArray); $j < $m; $j++) {

				// go through each route and filter out nonmatching entries one by one
				if (
					isset($routesArray[$j][1][$i][0]) &&
					isset($pathArray[$i]) &&
					$routesArray[$j][1][$i][0] != '$' &&
					$routesArray[$j][1][$i] !== $pathArray[$i]
				) {
					unset($routesArray[$j]);
					$routesArray = array_values($routesArray);
					$j--;
					$m--;
					continue;
				}
			}
		}

		// now filter out all leftover routes with non-variable entries
		for ($j=0, $m=sizeof($routesArray); $j < $m; $j++) {

			if (sizeof($routesArray[$j][1]) > $i) {
				for ($i2 = $i, $m2 = sizeof($routesArray[$j][1]); $i2 < $m2; $i2++) { 
					if ($routesArray[$j][1][$i2][0] != '$') {
						unset($routesArray[$j]);
						$routesArray = array_values($routesArray);
						$j--;
						$m--;
						if ($j < 0) {
							break;
						}
						continue;
					}
				}
			}
		}
		
		if (empty($routesArray)) {
			// we did not find a single matching route...
			return false;
		}
		$routesArray = array_values($routesArray);

		// we excecute the first matching route!
		$controller = $routesArray[0][0];
		$route = $routesArray[0][1];

		$variables = array();
		$i = 0;
		foreach ($route as $part) {
			$i++;
			if (!empty($part[0]) && $part[0] === '$') {
				$variables[$i] = substr($part, 1);
			}
		}

		$variables_out = array();
		$pathArray = explode('/', $path);
		$i = 0;
		foreach ($pathArray as $part) {
			$i++;
			if (empty($route[$i-1]) || !isset($variables[$i]) && $part !== $route[$i-1]) {
				continue; // route does not match
			}
			if (!empty($route[$i-1][0]) && $route[$i-1][0] === '$') {
				$variables_out[$variables[$i]] = $part;
			}
		}
		apcu_store(urlencode($path), [$controller, $variables_out], 900);
		return $this->dispatch($controller, $variables_out);
	}

	private function dispatch($callable, $params = null) {

		if (is_object($callable) && ($callable instanceof Closure)) {
			return $callable($params);
		}

		if (is_string($callable)) {
			if (($pos = strpos($callable, '::')) > 0) {
				// call static method
				return call_user_func($callable, $params);
			} elseif (($pos = strpos($callable, ':')) > 0) {
				// instantiate object and call public method
				$object = substr($callable, 0, $pos);
				$function = substr($callable, $pos+1);
				$instance = new $object;
				return $instance->$function($params);
			}
			// else, its probably a simple function
			return call_user_func($callable, $params);
		}
	}
}

